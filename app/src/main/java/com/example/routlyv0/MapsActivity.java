package com.example.routlyv0;

//importaciones basicas android
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.os.Bundle;
//importaciones extra para posicion usuario
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;
//
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.routlyv0.Modelos.InterestPoint;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

//
//importaciones para ver el mapa
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

//importaciones extra para posicion usuario
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;

import java.util.ArrayList;
import java.util.List;


public class MapsActivity extends Activity implements OnMapReadyCallback, PermissionsListener{

    private MapView mapView;

    public static final String SOURCE_ID = "mapbox.poi";

    //lo que añado para posicion usuario
    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private List<Point> routeCoordinates;

    private GeoJsonSource source;
    private FeatureCollection featureCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //el token de acceso va aqui
        Mapbox.getInstance(this, getString(R.string.access_token));
        //esto contiene el MapView en XML y debe de ser llemado despues de configurar el token
        setContentView(R.layout.activity_maps);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {

        MapsActivity.this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(Style.OUTDOORS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                Intent intentFromMain = getIntent();
                List<InterestPoint> puntos = (List<InterestPoint>) intentFromMain.getSerializableExtra("puntos");
                initRouteCoordinates(puntos);
                enableLocationComponent(style);

                // Create the LineString from the list of coordinates and then make a GeoJSON
                // FeatureCollection so we can add the line to our map as a layer.
                featureCollection = FeatureCollection.fromFeatures(new Feature[] {Feature.fromGeometry(LineString.fromLngLats(routeCoordinates))});
                style.addSource(new GeoJsonSource("line-source", featureCollection));

                List<Feature> markerCoordinates = new ArrayList<>();

                for (Point punto : routeCoordinates){
                    markerCoordinates.add(Feature.fromGeometry(
                            punto));
                }
                style.addSource(new GeoJsonSource("marker-source",
                        FeatureCollection.fromFeatures(markerCoordinates)));

                // The layer properties for our line. This is where we make the line dotted, set the
                // color, etc.
                LineLayer lineLayer = new LineLayer("linelayer", "line-source").withProperties(
                        PropertyFactory.lineDasharray(new Float[] {0.01f, 2f}),
                        PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                        PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
                        PropertyFactory.lineWidth(5f),
                        PropertyFactory.lineColor(Color.parseColor("#e55e5e"))
                );

                style.addImage("marker-icon-id",
                        BitmapFactory.decodeResource(
                                MapsActivity.this.getResources(), R.drawable.mapbox_marker_icon_default));


                SymbolLayer symbolLayer = new SymbolLayer("layer-id", "marker-source");
                symbolLayer.withProperties(
                        PropertyFactory.iconImage("marker-icon-id")
                );
                style.addLayer(symbolLayer);

                style.addLayer(lineLayer);
            }
        });
        /*mapboxMap.setOnMapClickListener(new MapboxMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng point) {
                PointF screenPoint = mapboxMap.getProjection().toScreenLocation(point);
                List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, "my.layer.id");
                if (!features.isEmpty()) {
                    Feature selectedFeature = features.get(0);
                    String title = selectedFeature.getStringProperty("title");
                    Toast.makeText(context, "You selected " + title, Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle){
        //verificar si los permisos estan concedidos y si no pedirlos
        if (PermissionsManager.areLocationPermissionsGranted(this)){
            //Obtener una instancia del componente
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

            //Activar con opciones
            locationComponent.activateLocationComponent(LocationComponentActivationOptions.builder(this,loadedMapStyle).build());

            //Hacer el componente visible
            locationComponent.setLocationComponentEnabled(true);

            //Configuarar el tipo de camara del componente
            locationComponent.setCameraMode(CameraMode.TRACKING);

            //Configurar el renderizador del componente
            locationComponent.setRenderMode(RenderMode.COMPASS);
        }else{
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        permissionsManager.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain){
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted){
        if(granted){
            mapboxMap.getStyle(new Style.OnStyleLoaded(){
                @Override
                public void onStyleLoaded(@NonNull Style style){
                    enableLocationComponent(style);
                }
            });
        }else{
            Toast.makeText(this,R.string.user_location_permission_not_granted,Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    @SuppressWarnings( {"MissingPermission"} )
    protected void onStart(){
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /*private void setupSource() {
        source = new GeoJsonSource(SOURCE_ID, featureCollection);
        mapboxMap.addSource(source);
        Style s = mapboxMap.getStyle();
        s.addSource(source);
        mapboxMap.
    }

    private void refreshSource() {
        if (source != null && featureCollection != null) {
            source.setGeoJson(featureCollection);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        refreshSource();
    }*/
    private void initRouteCoordinates(List<InterestPoint> puntos) {
        // Create a list to store our line coordinates.
        routeCoordinates = new ArrayList<>();
        for (InterestPoint punto : puntos){
            if (punto.getSelected()){
                //Location l = new Location();
                routeCoordinates.add(Point.fromLngLat(punto.getLongitud(), punto.getLatitud()));
            }
        }

    }


}
