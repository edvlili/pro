package com.example.routlyv0.Modelos;


import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

//esta clase la uso para devolver dos objetos de la misma  funcion.
//Imprescindible para BLACK BOX
//NO BORRAR
public class Pareja{
    private ArrayList<InterestPoint> seleccionados;
    private List<Point> coordPuntos;

    public Pareja(ArrayList<InterestPoint> seleccionados,List<Point> coordPuntos){
        this.coordPuntos=coordPuntos;
        this.seleccionados=seleccionados;
    }

    public ArrayList<InterestPoint> getSeleccionados(){
        return seleccionados;
    }

    public List<Point> getCoordPuntos() {
        return coordPuntos;
    }
}