package com.example.routlyv0.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Eduard on 29/04/2019.
 */
@ParseClassName("puntos_conexion")
public class ConectionPoint extends ParseObject {

    public String getCiudad() {
        return  getString("ciudad");
    }

    public void setCiudad(String ciudad) {
        put("ciudad",ciudad);
    }

    public double getLatitud() {
        return  getDouble("latitud");
    }

    public void setLatitud(double latitud) {
        put("latitud",latitud);
    }

    public double getLongitud() {
        return getDouble("longitud");
    }

    public void setLongitud(double longitud) {
        put("longitud",longitud);
    }
}
