package com.example.routlyv0.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Comparator;

@ParseClassName("puntos_interes")
public class InterestPoint extends ParseObject{

    public InterestPoint() {
    }

    public String getDescripcion(){return getString("descripcion");}

    public void setDescripcion(String descripcion){put("descripcion",descripcion);}

    public String getHorario(){return getString("horario");}

    public void setHorario(String horario){put("horario",horario);}

    public String getCiudad() {
        return  getString("ciudad");
    }

    public void setCiudad(String ciudad) {
        put("ciudad",ciudad);
    }

    public double getLatitud() {
        return  getDouble("latitud");
    }

    public void setLatitud(double latitud) {
        put("latitud",latitud);
    }

    public double getLongitud() {
       return getDouble("longitud");
    }

    public void setLongitud(double longitud) {
        put("longitud",longitud);
    }

    public String getNombre() {return getString("nombre");
    }

    public void setNombre(String nombre) {
        put("nombre",nombre);
    }

    public void setSelected(boolean selected){
        put("isSelected",selected);
    }

    public boolean getSelected(){
        return getBoolean("isSelected");
    }

    @Override
    public String toString() {
        return this.getNombre()+" "+this.getLatitud()+" "+this.getLongitud();
    }

    public static Comparator<InterestPoint> interestPointComparator = new Comparator<InterestPoint>() {

        public int compare(InterestPoint f1, InterestPoint f2) {

            String obj1 = f1.getNombre();
            String obj2 = f2.getNombre();
            if (obj1 == obj2) {
                return 0;
            }
            if (obj1 == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            return obj1.compareTo(obj2);

        }};

}
