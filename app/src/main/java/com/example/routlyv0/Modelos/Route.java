package com.example.routlyv0.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("rutas")
public class Route  extends ParseObject {

    public Route() {
    }

    public double getUser() {
        return  getInt("id_u");
    }

    public void setUser(double id_u) {
        put("id_u",id_u);
    }

    public double getRoute() {
        return  getInt("id_r");
    }

    public void setRoute(double id_r) {
        put("id_r",id_r);
    }

    public String getCiudad() {return getString("ciudad");}

    public void setCiudad(String ciudad) {
        put("ciudad",ciudad);
    }

    public double getLatitud() {return  getDouble("latitud");}

    public void setLatitud(double latitud) {put("latitud",latitud);}

    public double getLongitud() {return getDouble("longitud");}

    public void setLongitud(double longitud) {put("longitud",longitud);}





    @Override
    public String toString() {
        return this.getCiudad()+" "+this.getLatitud()+" "+this.getLongitud();
    }
}
