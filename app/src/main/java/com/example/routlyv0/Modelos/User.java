package com.example.routlyv0.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("usuarios")
public class User extends ParseObject {

    public User() {
    }

    public int getUser() {
        return  getInt("id_u");
    }

    public void setUser(int id_u) {
        put("id_u",id_u);
    }

    public String getNombre() {return getString("nombre");}

    public void setNombre(String nombre) {
        put("nombre",nombre);
    }

    public String getCorreo() {return getString("correo");}

    public void setCorreo(String correo) {
        put("correo",correo);
    }

    public String getPassword() {return getString("pass");}

    public void setPassword(String pass) {
        put("pass",pass);
    }

    @Override
    public String toString() {
        return this.getUser()+" "+this.getNombre()+" "+this.getCorreo();
    }
}
