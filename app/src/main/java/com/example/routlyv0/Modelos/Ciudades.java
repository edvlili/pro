package com.example.routlyv0.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@ParseClassName("ciudades")
public class Ciudades extends ParseObject {
    Ciudades() {
    }

    public String getCiudad() {
        return getString("ciudad");
    }

    public void setCiudad(String ciudad) {
        put("ciudad", ciudad);
    }
}
