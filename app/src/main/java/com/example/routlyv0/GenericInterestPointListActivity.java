package com.example.routlyv0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.routlyv0.Controladores.MyAdapter;
import com.example.routlyv0.Modelos.InterestPoint;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

public class GenericInterestPointListActivity extends ListActivity{

    private static final int SHOW_SUBACTIVITY = 1;
    SwipeRefreshLayout mSwipeRefreshLayout;

    ListView list;
    MyAdapter todoItemsAdapter;
    TravelPoint tpa;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        tpa = (TravelPoint)getApplicationContext();

        Intent intentFromMain = getIntent();
        String ciudad = intentFromMain.getStringExtra("ciudad");

        getServerList(ciudad);

        todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
        setListAdapter(todoItemsAdapter);

    }

    public void getServerList(String Ciudad) {

        ParseQuery<InterestPoint> query = ParseQuery.getQuery("puntos_interes");
        System.out.println(Ciudad);
        query.whereEqualTo("ciudad",Ciudad);
        query.findInBackground(new FindCallback<InterestPoint>() {
            public void done(List<InterestPoint> objects, ParseException e) {
                if (e == null) {
                    //System.out.println("Tamaño resultado query " +objects.size());
                    if (objects.size()>0) {
                        tpa.pointList=objects;
                    }else{
                        System.out.println("Esta ciudad no tiene puntos de interes");
                        tpa.pointList=new ArrayList<>();
                    }

                    if (todoItemsAdapter==null) {
                        todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");
                    }
                    else
                    {
                        todoItemsAdapter.notifyDataSetChanged();
                        Log.v("object query server:", "todoItemsAdapter= notifyDataSetChanged");

                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            InterestPoint item = (InterestPoint) list.getItemAtPosition(position);
                            Bundle bundle = new Bundle();
                            bundle.putInt("position", position);
                            bundle.putDouble("latitud", item.getLatitud());
                            bundle.putDouble("longitud", item.getLongitud());
                            Intent intent = new Intent(tpa, DisplayActivity.class);
                            intent.putExtras(bundle);
                            startActivityForResult(intent, SHOW_SUBACTIVITY);
                        }
                    });
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.v("query OK ", "getServerList()");
                } else {
                    Log.v("error query, reason: " + e.getMessage(), "getServerList()");
                    Toast.makeText(
                            getBaseContext(),
                            "getServerList(): error  query, reason: " + e.getMessage(),
                            LENGTH_SHORT).show();
                }
            }
        });
    }
}
