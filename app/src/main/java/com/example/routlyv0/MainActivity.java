package com.example.routlyv0;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;

import com.example.routlyv0.Controladores.MyAdapter;

import com.example.routlyv0.Modelos.Ciudades;
import com.example.routlyv0.Modelos.ConectionPoint;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Point;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.FindCallback;
import android.support.v4.widget.SwipeRefreshLayout;

import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.routlyv0.Modelos.InterestPoint;
import com.parse.SaveCallback;

import android.widget.Button;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends ListActivity {

    private static final int SHOW_SUBACTIVITY = 1;
    private static final int SHOW_ADDACTIVITY = 2;
    private static final int SHOW_LACTIVITY = 3;
    private static final int SHOW_SACTIVITY = 4;
    static public final int REQUEST_LOCATION = 1;

    ListView list;
    MyAdapter todoItemsAdapter;
    TravelPoint tpa;
    InterestPoint aInterestPoint;
    ConectionPoint aConectionPoint;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Button btnselect,btndeselect,btnnext,toMap;
    double tuUbi_lat=42.812526;
    double tuUbi_lon=-1.6457745;
    int permissionCheck;
    TextView tuUbi;
    TextView citat;
    private LocationManager locManager;
    private Location loc;
    int id_u;
    private String ciudad = "no ciudad";
    private Ciudades ciu;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout)  findViewById(R.id.activity_main_swipe_refresh_layout);
        list = this.getListView();
        tpa = (TravelPoint)getApplicationContext();

        //tuUbi = ((TextView) findViewById(R.id.tuUbi));
        //getServerList();
        //GPS
        permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheck== PackageManager.PERMISSION_DENIED){
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        gps();

        if (ciudad!="no ciudad"){
            getServerList(ciudad);
        }
        //tuUbi = ((TextView) findViewById(R.id.tuUbi));

        //System.out.println( "el valor de" + tuUbi_lat + " " + tuUbi_lon);

        //estado inicial de la lista todos los campos de isSelected a false
        //tpa.pointList=getModel(false,tpa.pointList);
        //todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
        //setListAdapter(todoItemsAdapter);

        //botones y cosas
        btnselect = (Button) findViewById(R.id.select);
        btndeselect = (Button) findViewById(R.id.deselect);
        btnnext = (Button) findViewById(R.id.next);
        toMap = (Button) findViewById(R.id.toMap);

        btnselect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tpa.pointList=getModel(true,tpa.pointList);
                todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
                setListAdapter(todoItemsAdapter);
            }
        });

        btndeselect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tpa.pointList=getModel(false,tpa.pointList);
                todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
                setListAdapter(todoItemsAdapter);
            }
        });

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,CityListActivity.class);
                intent.putExtra("puntos", (Serializable) tpa.pointList);
                startActivity(intent);
            }
        });

        toMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,NavigationActivity.class);
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (todoItemsAdapter==null){
                    getServerList(ciudad);
                }else {
                    todoItemsAdapter.clear();
                    todoItemsAdapter = null;
                    getServerList(ciudad);
                }
            }
        });

    }

    //cuando pulse el boton del mapa que me lleve a la vista del mapa
    public void showMap(MainActivity view){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("puntos", (Serializable) tpa.pointList);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);

        MenuItem myMenuItem = menu.findItem(R.id.action_menu);

        getMenuInflater().inflate(R.menu.submenu_principal, myMenuItem.getSubMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_gps:{
                gps();
                break;
            }
            case R.id.action_login: {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, SHOW_LACTIVITY);
                break;
            }

            case R.id.action_map:{
                showMap(this);
                break;
            }

            case R.id.action_signin: {
                Intent intent = new Intent(this, SigninActivity.class);
                startActivityForResult(intent, SHOW_SACTIVITY);
                break;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            /*if (requestCode == SHOW_SUBACTIVITY)
            {
                Bundle bundle = data.getExtras();
                String name= bundle.getString("name");
                int position= bundle.getInt("position");
                InterestPoint item = (InterestPoint) list.getItemAtPosition(position);
                item.setNombre(name);
                item.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            //    Toast.makeText(
                            //            getBaseContext(),
                            //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                            //            Toast.LENGTH_SHORT).show();
                            todoItemsAdapter.notifyDataSetChanged();
                            Log.v("object udpate server:", "update()");
                        } else {
                            Log.v("update failed, reason: "+ e.getMessage(), "update()");
                            Toast.makeText(
                                    getBaseContext(),
                                    "update(): Object udpate failed, reason: "+ e.getMessage(), LENGTH_SHORT).show();
                        }
                    }

                });
            }

            else if (requestCode == SHOW_ADDACTIVITY)
            {
                Bundle bundle = data.getExtras();
                Double latitud = bundle.getDouble("latitud");
                Double longitud = bundle.getDouble("longitud");
                newParseObject("",latitud,longitud);

            }
*/
        }
    }

    public void newParseObject(String name, Double latitud, Double longitud) {

        aInterestPoint = new InterestPoint();
        aInterestPoint.setNombre(name);
        aInterestPoint.setLatitud(latitud);
        aInterestPoint.setLongitud(longitud);

        aInterestPoint.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //    Toast.makeText(
                    //            getBaseContext(),
                    //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                    //            Toast.LENGTH_SHORT).show();
                    tpa.pointList.add(aInterestPoint);
                    todoItemsAdapter.notifyDataSetChanged();
                    Log.v("object saved in server:", "newParseObject()");
                } else {
                    Log.v("save failed, reason: "+ e.getMessage(), "newParseObject()");
                    Toast.makeText(
                            getBaseContext(),
                            "newParseObject(): Object save failed  to server, reason: "+ e.getMessage(), LENGTH_SHORT).show();
                }
            }

        });
    }

    public void newConectionObject(String name, Double latitud, Double longitud) {

        aConectionPoint = new ConectionPoint();
        aConectionPoint.setCiudad(name);
        aConectionPoint.setLatitud(latitud);
        aConectionPoint.setLongitud(longitud);

        aConectionPoint.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //    Toast.makeText(
                    //            getBaseContext(),
                    //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                    //            Toast.LENGTH_SHORT).show();
                    //tpa.pointList.add(aInterestPoint);
                    //todoItemsAdapter.notifyDataSetChanged();
                    Log.v("object saved in server:", "newParseObject()");
                } else {
                    Log.v("save failed, reason: "+ e.getMessage(), "newParseObject()");
                    Toast.makeText(
                            getBaseContext(),
                            "newParseObject(): Object save failed  to server, reason: "+ e.getMessage(), LENGTH_SHORT).show();
                }
            }

        });
    }

    public void getServerList(String Ciudad) {
        //gps();
        ParseQuery<InterestPoint> query = ParseQuery.getQuery("puntos_interes");
        System.out.println(Ciudad);
        query.whereEqualTo("ciudad",Ciudad);
        query.findInBackground(new FindCallback<InterestPoint>() {
            public void done(List<InterestPoint> objects, ParseException e) {
                if (e == null) {
                    //System.out.println("Tamaño resultado query " +objects.size());
                    if (objects.size()>0) {
                        Toast.makeText( getBaseContext(),"Te encuentras en: " + ciudad, Toast.LENGTH_SHORT).show();
                        tpa.pointList=objects;
                    }else{
                        //System.out.println("Esta ciudad no tiene puntos de interes");
                        Toast.makeText( getBaseContext(),"Por favor, Ubícate", Toast.LENGTH_SHORT).show();
                        tpa.pointList=new ArrayList<>();
                    }

                    if (todoItemsAdapter==null) {
                        todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");
                    }
                    else
                    {
                        todoItemsAdapter.notifyDataSetChanged();
                        Log.v("object query server:", "todoItemsAdapter= notifyDataSetChanged");

                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            InterestPoint item = (InterestPoint) list.getItemAtPosition(position);
                            List<InterestPoint> aux = new ArrayList<>();
                            aux.add(item);
                            Intent intent = new Intent(tpa, DisplayActivity.class);
                            intent.putExtra("punto",(Serializable)aux);
                            intent.putExtra("position",position);
                            startActivityForResult(intent, SHOW_SUBACTIVITY);
                        }
                    });
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.v("query OK ", "getServerList()");
                } else {
                    Log.v("error query, reason: " + e.getMessage(), "getServerList()");
                    Toast.makeText(
                            getBaseContext(),
                            "getServerList(): error  query, reason: " + e.getMessage(),
                            LENGTH_SHORT).show();
                }
            }
        });
    }

    private List<InterestPoint> getModel(boolean isSelect,List<InterestPoint> puntos){
        for (InterestPoint ip : puntos){
            ip.setSelected(isSelect);
        }
        return puntos;
    }


   public void gps (){
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) MainActivity.this.getSystemService( Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                //tuUbi.setText(" " + location.getLatitude() + " " + location.getLongitude());
                tuUbi_lat = location.getLatitude();
                tuUbi_lon = location.getLongitude();
                busquedaGocode(tuUbi_lat,tuUbi_lon);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}

        };
        permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        busquedaGocode(tuUbi_lat,tuUbi_lon);
    }

    private void busquedaGocode(double tuUbi_lat, double tuUbi_lon){
        try{
            //Peticion GEOCODE de MAPBOX
            MapboxGeocoding cliente = MapboxGeocoding.builder().accessToken(getString(R.string.access_token)).query(Point.fromLngLat(tuUbi_lon,tuUbi_lat)).geocodingTypes(GeocodingCriteria.TYPE_PLACE).mode(GeocodingCriteria.MODE_PLACES).build();
            cliente.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                    String city;
                    List<CarmenFeature> results = response.body().features();
                    if(results.size()>0){
                        //Con pillar el primero de ellos nos vale
                        CarmenFeature feature = results.get(0);
                        city = feature.placeName();
                        city = city.substring( 0, city.indexOf(","));
                        if (city.equals(ciudad)){
                            //NO HACER NADA
                        }else{
                            ciudad = city;
                            newConectionObject(ciudad,tuUbi_lat,tuUbi_lon);
                            System.out.println("La ciudad dentro del geocode:" +ciudad);
                            getServerList(ciudad);
                        }


                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_results, LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }
}

