package com.example.routlyv0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.routlyv0.Modelos.Route;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;
import java.util.function.Predicate;

public class RouteActivity extends ListActivity {
    private static final int SHOW_SUBACTIVITY = 1;
    private static final int SHOW_ADDACTIVITY = 2;

    ListView list;
    ArrayAdapter<Route> todoItemsAdapter;
    TravelPoint tpr;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int contador;
    int id_u = 1;
    Intent intent;
    Route aRoutes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_route );

        mSwipeRefreshLayout = (SwipeRefreshLayout)  findViewById(R.id.activity_main_swipe_refresh_layout);

        list = this.getListView();
        tpr = (TravelPoint)getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id_u = bundle.getInt("id_u");
        }

        getServerList();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                todoItemsAdapter.clear();
                todoItemsAdapter=null;
                getServerList();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_user, menu);

        MenuItem myMenuItem = menu.findItem(R.id.action_user);

        getMenuInflater().inflate(R.menu.submenu_user, myMenuItem.getSubMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_new: {
                Intent intent = new Intent(tpr, AddRouteActivity.class);
                startActivityForResult(intent, SHOW_ADDACTIVITY);
                break;
            }

            case R.id.action_get: {
                getServerList();
                break;
            }

            case R.id.action_logout: {
                break;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SHOW_ADDACTIVITY)
            {
                Bundle bundle = data.getExtras();
                String ciudad = bundle.getString("ciudad");
                Double latitud = bundle.getDouble("latitud");
                Double longitud = bundle.getDouble("longitud");
                try {
                    newParseObject(ciudad,latitud,longitud);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    public void newParseObject(String ciudad, Double latitud, Double longitud) throws ParseException {
        ParseQuery<Route> query = ParseQuery.getQuery("rutas");
        query.orderByDescending( "id_r" );
        double id_r = query.getFirst().getRoute()+1;
        aRoutes = new Route();
        aRoutes.setUser(id_u);
        aRoutes.setRoute(id_r);
        aRoutes.setCiudad(ciudad);
        aRoutes.setLatitud(latitud);
        aRoutes.setLongitud(longitud);

        aRoutes.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //    Toast.makeText(
                    //            getBaseContext(),
                    //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                    //            Toast.LENGTH_SHORT).show();
                    tpr.routeList.add(aRoutes);
                    todoItemsAdapter.notifyDataSetChanged();
                    Log.v("object saved in server:", "newParseObject()");
                } else {
                    Log.v("save failed, reason: "+ e.getMessage(), "newParseObject()");
                    Toast.makeText(
                            getBaseContext(),
                            "newParseObject(): Object save failed  to server, reason: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    public void getServerList() {
        ParseQuery<Route> query = ParseQuery.getQuery("rutas");
        query.findInBackground(new FindCallback<Route>() {
            public void done(List<Route> objects, ParseException e) {

                //valor id
                //intent.getIntExtra("id_u",id_u );

                if (e == null) {
                    tpr.routeList = objects;
                    tpr.routeList.removeIf( new Predicate<Route>() {
                        @Override
                        public boolean test(Route route) {
                            return route.getUser()!=id_u;
                        }
                    } );
                    if (todoItemsAdapter==null) {
                        todoItemsAdapter = new ArrayAdapter<Route>(getApplicationContext(), R.layout.row_route, R.id.listText, tpr.routeList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");
                        /*todoItemsAdapter = new MyAdapter(getApplicationContext(), tpr.pointList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");*/
                    }
                    else
                    {
                        todoItemsAdapter.notifyDataSetChanged();
                        Log.v("object query server:", "todoItemsAdapter= notifyDataSetChanged");

                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Route item = (Route) list.getItemAtPosition(position);
                            Bundle bundle = new Bundle();
                            bundle.putInt("position", position);
                            bundle.putString("ciudad", item.getCiudad());
                            bundle.putDouble("id_r", item.getRoute());
                            bundle.putDouble("latitud", item.getLatitud());
                            bundle.putDouble("longitud", item.getLongitud());
                            Intent intent = new Intent(tpr, PointActivity.class);
                            intent.putExtras(bundle);
                            startActivityForResult(intent, SHOW_SUBACTIVITY);
                        }
                    });
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.v("query OK ", "getServerList()");
                } else {
                    Log.v("error query, reason: " + e.getMessage(), "getServerList()");
                    Toast.makeText(
                            getBaseContext(),
                            "getServerList(): error  query, reason: " + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
