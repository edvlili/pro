package com.example.routlyv0;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.routlyv0.Modelos.InterestPoint;

import java.util.List;

public class DisplayActivity extends Activity {

    InterestPoint punto;
    List<InterestPoint> aux;
	int position;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		Intent intent = getIntent();
		aux = (List<InterestPoint>) intent.getSerializableExtra("punto");
		punto = aux.get(0);
		position= intent.getIntExtra("position",0);
        TextView textView =((TextView) findViewById(R.id.text_longitud));
		textView.setTextSize(40);
		textView.setText(String.valueOf(punto.getLongitud()));
		TextView textView2 =((TextView) findViewById(R.id.text_latitud));
		textView2.setTextSize(40);
		textView2.setText(String.valueOf(punto.getLatitud()));
		TextView descripcion = ((TextView)findViewById(R.id.descripcionBox));
		descripcion.setText(punto.getDescripcion());
		TextView horario = ((TextView)findViewById(R.id.horarioBox));
		horario.setText(punto.getHorario());


	}

	public void saveLocationName(View view) {

		Bundle bundle = new Bundle();
		bundle.putInt("position", position);
        EditText editText = (EditText) findViewById(R.id.edit_point_name);
        String point_name = editText.getText().toString();
        bundle.putString("name", point_name);
        Intent intent = new Intent();
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}
}
