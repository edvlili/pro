package com.example.routlyv0;

import android.app.Application;

import com.example.routlyv0.Modelos.ConectionPoint;
import com.example.routlyv0.Modelos.InterestPoint;
import com.example.routlyv0.Modelos.Route;
import com.example.routlyv0.Modelos.User;
import com.parse.Parse;
import com.parse.ParseObject;
import com.example.routlyv0.Modelos.Ciudades;

import java.util.ArrayList;
import java.util.List;

public class TravelPoint extends Application {


    //guardamos los puntos de interes de la ubicacion actual
    public List<InterestPoint> pointList = new ArrayList<>();
    //no es necesario
    public List<InterestPoint> selectedList = new ArrayList<>();
    //lista de rutas( version Ricky)
    public List<Route> routeList = new ArrayList<>();
    //lista de Ciudades disponibles con puntos de interes
    public List<Ciudades> ciudadesList = new ArrayList<>();
    //lista de usuarios?? hmmm solo obtenemos el usuario que necesitamos ("esta arreglado en la query")
    //public List<User> userList = new ArrayList<>();
    public User usuario;
    public List<User> userList = new ArrayList<>();


    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(InterestPoint.class);
        ParseObject.registerSubclass( Route.class);
        ParseObject.registerSubclass( User.class);
        ParseObject.registerSubclass(Ciudades.class);
        ParseObject.registerSubclass(ConectionPoint.class);

		Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
            .applicationId("myAppId") //si no has cambiado APP_ID, sino pon el valor de APP_ID
				  .clientKey("empty")
                .server(getString(R.string.BBDD_address))   // '/' important after 'parse'
                .build());

    }
}
