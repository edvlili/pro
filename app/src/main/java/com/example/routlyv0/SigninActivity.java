package com.example.routlyv0;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.example.routlyv0.Modelos.Route;
import com.example.routlyv0.Modelos.User;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class SigninActivity extends Activity {
    private static final int SHOW_SUBACTIVITY = 1;
    private static final int SHOW_SACTIVITY = 4;
    int id_u = 0;
    String usuario = "";
    String password;
    String password2;
    User aUser;
    ArrayAdapter<User> todoItemsAdapter;
    TravelPoint tpu;
    Bundle bundle;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin );

        tpu = (TravelPoint)getApplicationContext();

    }

    public void Signin(View view)throws ParseException {
        bundle = new Bundle();
        EditText user = (EditText) findViewById(R.id.user);
        EditText pass = (EditText) findViewById(R.id.pass);
        EditText pass2 = (EditText) findViewById(R.id.pass2);
        usuario = user.getText().toString();
        password = pass.getText().toString();
        password2 = pass2.getText().toString();

        //comprobacion passwords iguales
        //almacernar usuario con id, ultimo mas 1

        ParseQuery<User> query = ParseQuery.getQuery("usuarios");
        query.orderByDescending( "id_u" );
        id_u = query.getFirst().getUser()+1;
        query.whereEqualTo("nombre", usuario);
        //whereGreaterThan  para int

        if (query.find().isEmpty()){
            //Toast.makeText(SigninActivity.this, "usuario no existe " + usuario , Toast.LENGTH_SHORT).show();
            if (password.compareTo( password2 )==0 && !password.isEmpty()){
                //Toast.makeText(getBaseContext(),"tu id sera: "+id_u, Toast.LENGTH_SHORT).show();
                Toast.makeText(getBaseContext(),"Usuario registrado", Toast.LENGTH_SHORT).show();
                newParseObject(id_u,usuario,"",password);
                bundle.putInt("id_u",id_u);
                bundle.putString("usuario", usuario);
                intent = new Intent(tpu, RouteActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, SHOW_SACTIVITY);
            }else
                Toast.makeText(getBaseContext(),"Las contraseñas NO son iguales o estan vacias", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(SigninActivity.this, "Usuario existente", Toast.LENGTH_SHORT).show();
        }
    }

    public void newParseObject(int id_u, String nombre, String correo, String password){
        bundle = new Bundle();
        aUser = new User();
        aUser.setUser(id_u);
        aUser.setNombre(nombre);
        aUser.setCorreo( correo );
        aUser.setPassword(password);

        aUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //    Toast.makeText(
                    //            getBaseContext(),
                    //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                    //            Toast.LENGTH_SHORT).show();
                    tpu.userList.add(aUser);
                    todoItemsAdapter.notifyDataSetChanged();
                    Log.v("object saved in server:", "newParseObject()");
                } else {
                    Log.v("save failed, reason: "+ e.getMessage(), "newParseObject()");
                    Toast.makeText(
                            getBaseContext(),
                            "newParseObject(): Object save failed  to server, reason: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }
}
