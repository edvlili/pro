package com.example.routlyv0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.routlyv0.Controladores.MyAdapter;
import com.example.routlyv0.Modelos.InterestPoint;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static android.widget.Toast.LENGTH_SHORT;

public class PointActivity extends ListActivity {

    private static final int SHOW_SUBACTIVITY = 1;
    private static final int SHOW_ADDACTIVITY = 2;

    ListView list;
    MyAdapter todoItemsAdapter;
    TravelPoint tpp;
    InterestPoint aInterestPoint;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Button btnselect,btndeselect,btnnext;
    int id_r;
    String ciudad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSwipeRefreshLayout = (SwipeRefreshLayout)  findViewById(R.id.activity_main_swipe_refresh_layout);
        list = this.getListView();
        tpp = (TravelPoint)getApplicationContext();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        ciudad = bundle.getString("ciudad");

        getServerList(ciudad);

        //estado inicial de la lista todos los campos de isSelected a false
        //tpa.pointList=getModel(false,tpa.pointList);
        //todoItemsAdapter = new MyAdapter(getApplicationContext(), tpa.pointList);
        //setListAdapter(todoItemsAdapter);

        //botones y cosas
        btnselect = (Button) findViewById(R.id.select);
        btndeselect = (Button) findViewById(R.id.deselect);
        btnnext = (Button) findViewById(R.id.next);

        btnselect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tpp.pointList=getModel(true,tpp.pointList);
                todoItemsAdapter = new MyAdapter(getApplicationContext(), tpp.pointList);
                setListAdapter(todoItemsAdapter);
            }
        });

        btndeselect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tpp.pointList=getModel(false,tpp.pointList);
                todoItemsAdapter = new MyAdapter(getApplicationContext(), tpp.pointList);
                setListAdapter(todoItemsAdapter);
            }
        });

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( PointActivity.this,CityListActivity.class);
                intent.putExtra("puntos", (Serializable) tpp.pointList);
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                todoItemsAdapter.clear();
                todoItemsAdapter=null;
                getServerList(ciudad);
            }
        });

    }

    //cuando pulse el boton del mapa que me lleve a la vista del mapa
    public void showMap(PointActivity view){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("puntos", (Serializable) tpp.pointList);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_route, menu);

        MenuItem myMenuItem = menu.findItem(R.id.action_route);

        getMenuInflater().inflate(R.menu.submenu_route, myMenuItem.getSubMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_logout: {
                break;
            }

            case R.id.action_new: {
                Intent intent = new Intent(tpp, AddPointActivity.class);
                startActivityForResult(intent, SHOW_ADDACTIVITY);
                break;
            }

            case R.id.action_map:{
                showMap(this);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SHOW_SUBACTIVITY)
            {
                Bundle bundle = data.getExtras();
                String name= bundle.getString("name");
                int position= bundle.getInt("position");
                InterestPoint item = (InterestPoint) list.getItemAtPosition(position);
                item.setNombre(name);
                item.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            //    Toast.makeText(
                            //            getBaseContext(),
                            //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                            //            Toast.LENGTH_SHORT).show();
                            todoItemsAdapter.notifyDataSetChanged();
                            Log.v("object udpate server:", "update()");
                        } else {
                            Log.v("update failed, reason: "+ e.getMessage(), "update()");
                            Toast.makeText(
                                    getBaseContext(),
                                    "update(): Object udpate failed, reason: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                });
            }

            else if (requestCode == SHOW_ADDACTIVITY)
            {
                Bundle bundle = data.getExtras();
                Double latitud = bundle.getDouble("latitud");
                Double longitud = bundle.getDouble("longitud");
                newParseObject("",latitud,longitud);

            }

        }
    }

    public void newParseObject(String name, Double latitud, Double longitud) {

        aInterestPoint = new InterestPoint();
        aInterestPoint.setNombre(name);
        aInterestPoint.setLatitud(latitud);
        aInterestPoint.setLongitud(longitud);

        aInterestPoint.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //    Toast.makeText(
                    //            getBaseContext(),
                    //            "newParseObject(): object saved in server: " + aInterestPoint.getObjectId(),
                    //            Toast.LENGTH_SHORT).show();
                    tpp.pointList.add(aInterestPoint);
                    todoItemsAdapter.notifyDataSetChanged();
                    Log.v("object saved in server:", "newParseObject()");
                } else {
                    Log.v("save failed, reason: "+ e.getMessage(), "newParseObject()");
                    Toast.makeText(
                            getBaseContext(),
                            "newParseObject(): Object save failed  to server, reason: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }
    public void getServerList(String Ciudad) {

        ParseQuery<InterestPoint> query = ParseQuery.getQuery("puntos_interes");
        System.out.println(Ciudad);
        query.whereEqualTo("ciudad",Ciudad);
        query.findInBackground(new FindCallback<InterestPoint>() {
            public void done(List<InterestPoint> objects, ParseException e) {
                if (e == null) {
                    //System.out.println("Tamaño resultado query " +objects.size());
                    if (objects.size()>0) {
                        tpp.pointList=objects;
                    }else{
                        System.out.println("Esta ciudad no tiene puntos de interes");
                        tpp.pointList=new ArrayList<>();
                    }

                    if (todoItemsAdapter==null) {
                        todoItemsAdapter = new MyAdapter(getApplicationContext(), tpp.pointList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");
                    }
                    else
                    {
                        todoItemsAdapter.notifyDataSetChanged();
                        Log.v("object query server:", "todoItemsAdapter= notifyDataSetChanged");

                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            InterestPoint item = (InterestPoint) list.getItemAtPosition(position);
                            List<InterestPoint> aux = new ArrayList<>();
                            aux.add(item);
                            Intent intent = new Intent(tpp, DisplayActivity.class);
                            intent.putExtra("punto",(Serializable)aux);
                            intent.putExtra("position",position);
                            startActivityForResult(intent, SHOW_SUBACTIVITY);
                        }
                    });
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.v("query OK ", "getServerList()");
                } else {
                    Log.v("error query, reason: " + e.getMessage(), "getServerList()");
                    Toast.makeText(
                            getBaseContext(),
                            "getServerList(): error  query, reason: " + e.getMessage(),
                            LENGTH_SHORT).show();
                }
            }
        });
    }

    private List<InterestPoint> getModel(boolean isSelect,List<InterestPoint> puntos){
        for (InterestPoint ip : puntos){
            ip.setSelected(isSelect);
        }
        return puntos;
    }


}

