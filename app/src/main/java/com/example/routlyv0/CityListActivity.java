package com.example.routlyv0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.routlyv0.Controladores.CityAdapter;
import com.example.routlyv0.Controladores.MyAdapter;
import com.example.routlyv0.Modelos.Ciudades;
import com.example.routlyv0.Modelos.InterestPoint;
import com.example.routlyv0.Modelos.Route;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CityListActivity extends ListActivity {
    private static final int SHOW_SUBACTIVITY = 1;
    private static final int SHOW_ADDACTIVITY = 2;

    ListView list;
    CityAdapter todoItemsAdapter;
    TravelPoint tpr;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_route );

        mSwipeRefreshLayout = (SwipeRefreshLayout)  findViewById(R.id.activity_main_swipe_refresh_layout);

        list = this.getListView();
        tpr = (TravelPoint)getApplicationContext();


        getServerList();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                todoItemsAdapter.clear();
                todoItemsAdapter=null;
                getServerList();
            }
        });

    }

    public void getServerList() {
        ParseQuery<Ciudades> query = ParseQuery.getQuery("ciudades");
        query.findInBackground(new FindCallback<Ciudades>() {
            public void done(List<Ciudades> objects, ParseException e) {
                if (e == null) {
                    tpr.ciudadesList = objects;
                    if (todoItemsAdapter==null) {
                        for (int i=0; i<tpr.ciudadesList.size();i++){
                            System.out.println(tpr.ciudadesList.get(i).getCiudad());
                        }
                        todoItemsAdapter = new CityAdapter(getApplicationContext(), tpr.ciudadesList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");
                        /*todoItemsAdapter = new MyAdapter(getApplicationContext(), tpr.pointList);
                        setListAdapter(todoItemsAdapter);
                        Log.v("object query server:", "todoItemsAdapter= null");*/
                    }
                    else
                    {
                        todoItemsAdapter.notifyDataSetChanged();
                        Log.v("object query server:", "todoItemsAdapter= notifyDataSetChanged");

                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //guardamos la ciudad en el intent y abrimos una nueva actividad en la que
                            //los puntos de interes de esa ciudad
                            Ciudades item = (Ciudades) list.getItemAtPosition(position);
                            Bundle bundle = new Bundle();
                            bundle.putString("ciudad", item.getCiudad());
                            Intent intent = new Intent(tpr, PointActivity.class);
                            intent.putExtras(bundle);
                            //comenzar actividad sin esperar resultado
                            startActivity(intent);
                        }
                    });
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.v("query OK ", "getServerList()");
                } else {
                    Log.v("error query, reason: " + e.getMessage(), "getServerList()");
                    Toast.makeText(
                            getBaseContext(),
                            "getServerList(): error  query, reason: " + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}