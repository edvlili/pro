package com.example.routlyv0;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddPointActivity extends Activity {

	double tuUbi_lat;
	double tuUbi_lon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addpoint);


	}

	public void saveLocation(View view) {
		Bundle bundle = new Bundle();
		/*Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		pamplona_lat= bundle.getDouble("latitud");
		pamplona_lon= bundle.getDouble("longitud");
		*/

		EditText editLatitud = (EditText) findViewById(R.id.edit_latitud);
		EditText editLongitud = (EditText) findViewById(R.id.edit_longitud);
		EditText editName = (EditText) findViewById(R.id.edit_point_name);
		String longitud = editLongitud.getText().toString();
		String latitud = editLatitud.getText().toString();
		String name = editName.getText().toString();
		double lon =  Double.valueOf(longitud);
		double lat =  Double.valueOf(latitud);
		bundle.putString("name", name);
		bundle.putDouble("longitud", lon);
		bundle.putDouble("latitud", lat);
		Intent intent = new Intent();
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}
}
