package com.example.routlyv0.Controladores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.routlyv0.Modelos.InterestPoint;
import com.example.routlyv0.R;

import java.util.List;

public class MyAdapter extends ArrayAdapter<InterestPoint> {

    private Context context;
    public static List<InterestPoint> puntos;
    //El contador esta para limitar el numero de selecciones a 24
    //Ah aqui es tambien donde tenemos el click listener de los checkbox
    private int contador =0;

    //para mejorar el rendimiento
    private static class ViewHolder{
        TextView nombre;
        TextView latitud;
        TextView longitud;
        CheckBox checkBox;
        TextView estado;
    }

    public MyAdapter(Context context, List<InterestPoint> puntos){
        super(context, R.layout.row_layout,puntos);
        this.context = context;
        this.puntos = puntos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //Obtener los datos de esta posicion
        InterestPoint p = getItem(position);
        //Comprobar si la vista eistente ya ha sido utilizada y si no inflar la vista
        final ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_layout,parent,false);
            //poblar la vista
            viewHolder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            viewHolder.latitud = (TextView) convertView.findViewById(R.id.latitud);
            viewHolder.longitud = (TextView) convertView.findViewById(R.id.longitud);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            viewHolder.estado = (TextView) convertView.findViewById(R.id.estado);
            //introducir el viewHolder en la nuevavista
            convertView.setTag(viewHolder);
        }else{
            //El viewHolder se recicla, obtener el viewHolder desde el tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //Poner los datos en el template utilizando el objeto
        viewHolder.nombre.setText(p.getNombre());
        viewHolder.latitud.setText(String.valueOf(p.getLatitud()));
        viewHolder.longitud.setText(String.valueOf(p.getLongitud()));
        viewHolder.checkBox.setChecked(puntos.get(position).getSelected());
        viewHolder.estado.setText(String.valueOf(puntos.get(position).getSelected()));

        viewHolder.checkBox.setTag(R.integer.check,convertView);
        viewHolder.checkBox.setTag(position);
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View tempview = (View) viewHolder.checkBox.getTag(R.integer.check);
                Integer pos = (Integer) viewHolder.checkBox.getTag();
                boolean checked = ((CheckBox)v).isChecked();
                if (checked && contador>=24){
                    ((CheckBox)v).setChecked(false);
                }else{
                    if(checked){
                        puntos.get(pos).setSelected(checked);
                        contador++;
                    }else{
                        contador--;
                    }
                }

            }
        });
        //Devolver la vista para mostrarla por pantalla
        return convertView;
    }

    @Override
    public int getViewTypeCount(){
        return 1;
    }

    @Override
    public int getItemViewType(int position){
        return position;
    }

    @Override
    public int getCount(){
        return puntos.size();
    }

    @Override
    public InterestPoint getItem(int position){
        return puntos.get(position);
    }

    public void resetContador(){
        contador= 0;
    }

    public void incrementContador(){
        if(!isContadorMax()){
            contador++;
        }
    }

    public boolean isContadorMax(){
        return contador>=24;
    }

}
