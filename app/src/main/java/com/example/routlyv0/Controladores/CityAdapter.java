package com.example.routlyv0.Controladores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.routlyv0.Modelos.Ciudades;
import com.example.routlyv0.R;

import java.util.List;

public class CityAdapter extends ArrayAdapter<Ciudades> {

    private Context context;
    public static List<Ciudades> puntos;
    //El contador esta para limitar el numero de selecciones a 24
    //Ah aqui es tambien donde tenemos el click listener de los checkbox
    private int contador =0;

    //para mejorar el rendimiento
    private static class ViewHolder{
        TextView nombre;
    }

    public CityAdapter(Context context, List<Ciudades> puntos){
        super(context, R.layout.row_route,puntos);
        this.context = context;
        this.puntos = puntos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //Obtener los datos de esta posicion
        Ciudades p = getItem(position);
        //Comprobar si la vista eistente ya ha sido utilizada y si no inflar la vista
        final ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_route,parent,false);
            //poblar la vista
            viewHolder.nombre = (TextView) convertView.findViewById(R.id.listText);
            //introducir el viewHolder en la nuevavista
            convertView.setTag(viewHolder);
        }else{
            //El viewHolder se recicla, obtener el viewHolder desde el tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //Poner los datos en el template utilizando el objeto
        viewHolder.nombre.setText(p.getCiudad());
        //Devolver la vista para mostrarla por pantalla
        return convertView;
    }

    @Override
    public int getViewTypeCount(){
        return 1;
    }

    @Override
    public int getItemViewType(int position){
        return position;
    }

    @Override
    public int getCount(){
        return puntos.size();
    }

    @Override
    public Ciudades getItem(int position){
        return puntos.get(position);
    }
}
