package com.example.routlyv0.Controladores;

import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.routlyv0.Modelos.InterestPoint;
import com.example.routlyv0.Modelos.Pareja;
import com.example.routlyv0.R;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.matrix.v1.models.MatrixResponse;
import com.mapbox.geojson.Point;
import com.mapbox.api.matrix.v1.MapboxMatrix;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlackBox {
    //30 minutos en segundos
    private float Tespera = 30 * 60;
    private ArrayList<InterestPoint> puntos;
    private ArrayList<InterestPoint> seleccionados;
    private List<Point> coordPuntos;
    private Point inicio;
    private DayOfWeek dia;
    List<Double[]> matrix;

    //Al constructor le pasamos TODOS los puntos. Tal y como salen de la lista de seleccionados
    public BlackBox(ArrayList<InterestPoint> puntos, InterestPoint inicio) {
        //Hacer que esta clase la instanciemos al iniciar la aplicacion y añadirla al context para hacerlo disponible a todas las activitys
        this.puntos = puntos;
        LocalDate fecha = LocalDate.now();
        this.dia = fecha.getDayOfWeek();
        this.inicio = Point.fromLngLat(inicio.getLongitud(), inicio.getLatitud());
    }

    public ArrayList<InterestPoint> generaRuta() {
        Pareja aux = getSelected(puntos);
        this.seleccionados = aux.getSeleccionados();
        this.coordPuntos = aux.getCoordPuntos();
        //añado la posicion del usuario como la primera posicion de la lista
        this.coordPuntos.add(0,inicio);


        MapboxMatrix directionsMatrixClient = MapboxMatrix.builder()
                .accessToken(Resources.getSystem().getString(R.string.access_token))
                .profile(DirectionsCriteria.PROFILE_WALKING)
                .coordinates(coordPuntos)
                .build();

        //EN CONSTRUCCION #########################################################################

        directionsMatrixClient.enqueueCall(new Callback<MatrixResponse>() {
            @Override
            public void onResponse(Call<MatrixResponse> call, Response<MatrixResponse> response) {
                matrix = response.body().durations();
            }

            @Override
            public void onFailure(Call<MatrixResponse> call, Throwable throwable) {
                Log.d("MatrixApiActivity", "onResponse onFailure");
            }
        });
        //##########################################################################################*/
        int punto_actual = 0;
        for (int i=0; i<matrix.size();i++){
 
        }


        return puntos;
    }

    private Pareja getSelected(ArrayList<InterestPoint> puntos) {
        ArrayList<InterestPoint> seleccionados = new ArrayList<>();
        ArrayList<Point> coordPuntos = new ArrayList<>();
        for (InterestPoint punto : puntos) {
            if (punto.getSelected()) {
                seleccionados.add(punto);
                coordPuntos.add( Point.fromLngLat(punto.getLongitud(), punto.getLatitud()));
            }
        }
        return new Pareja(seleccionados, coordPuntos);
    }
}
