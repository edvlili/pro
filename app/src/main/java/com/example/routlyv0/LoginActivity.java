package com.example.routlyv0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.routlyv0.Controladores.MyAdapter;
import com.example.routlyv0.Modelos.InterestPoint;
import com.example.routlyv0.Modelos.Route;
import com.example.routlyv0.Modelos.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;
import java.util.function.Predicate;

public class LoginActivity extends Activity {
	ListView list;
	ArrayAdapter<User> todoItemsAdapter;
	TravelPoint tpu;
	User users;
	int id_u = 0;
	String usuario = "";
	String password;

	private static final int SHOW_SUBACTIVITY = 1;
	private static final int SHOW_LACTIVITY = 3;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		tpu = (TravelPoint)getApplicationContext();

	}

	public void login(View view) throws ParseException {
		Bundle bundle = new Bundle();
		EditText user = (EditText) findViewById(R.id.user);
		EditText pass = (EditText) findViewById(R.id.pass);
		usuario = user.getText().toString();
		password = pass.getText().toString();

		//getServerList();

		ParseQuery<User> query = ParseQuery.getQuery("usuarios");
		query.whereEqualTo("nombre", usuario);
		//whereGreaterThan  para int

		if (query.find().isEmpty()){
			Toast.makeText(LoginActivity.this, "Usuario o password erroneos " + usuario , Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(LoginActivity.this, "Id de Usuario: " + query.getFirst().getUser(), Toast.LENGTH_SHORT).show();
			id_u = query.getFirst().getUser();
			bundle.putInt("id_u", id_u);
			bundle.putString("usuario", usuario);
			Intent intent = new Intent(tpu, RouteActivity.class);
			intent.putExtras(bundle);
			startActivityForResult(intent, SHOW_LACTIVITY);
		}
	}
}
